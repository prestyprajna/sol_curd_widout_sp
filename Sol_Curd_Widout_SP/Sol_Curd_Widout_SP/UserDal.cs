﻿using Sol_Curd_Widout_SP.Ef;
using Sol_Curd_Widout_SP.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Sol_Curd_Widout_SP
{
    public class UserDal
    {
        private UserDBEntities db = null;

        public UserDal()
        {
            db = new UserDBEntities();
        }

        public async Task<Boolean> InsertAsync(UserEntity userEntityObj)
        {
            try
            {
                var setQuery =
                db.tblUsers
                .Add(await this.Mapping(new tblUser(), userEntityObj));

                await db.SaveChangesAsync();

                return true;
            }
            catch (Exception)
            {

                throw;
            }
            
        }

        public async Task<Boolean> UpdateAsync(UserEntity userEntityObj)
        {
            try
            {
                tblUser tblUserObj = await this.GetDataByIdAsync(userEntityObj);

                await this.Mapping(tblUserObj, userEntityObj);

                await db.SaveChangesAsync();

                return true;                   
            }
            catch (Exception)
            {

                throw;
            }

        }

        public async Task<Boolean> DeleteAsync(UserEntity userEntityObj)
        {
            try
            {
                tblUser tblUserObj = await this.GetDataByIdAsync(userEntityObj);

                db.tblUsers.Remove(tblUserObj);

                await db.SaveChangesAsync();

                return true;
            }
            catch (Exception)
            {

                throw;
            }

        }

        private async Task<tblUser> Mapping(tblUser tblUserObj,UserEntity userEntityObj)
        {
            try
            {
                return await Task.Run(() =>
                {
                    tblUserObj.UserId = userEntityObj.UserId;
                    tblUserObj.FirstName = userEntityObj.FirstName;
                    tblUserObj.LastName = userEntityObj.LastName;

                    return tblUserObj;
                });
            }
            catch (Exception)
            {

                throw;
            }
                  
        }

        private async Task<tblUser> GetDataByIdAsync(UserEntity userEntityObj)
        {
            //return await Task.Run(()=>
            //{
            return await
                db.tblUsers
            ?.AsQueryable()
            .Where(
                (letblUserObj) => letblUserObj.UserId == userEntityObj.UserId
                )
            .FirstOrDefaultAsync();
            //})
        }


    }
}
