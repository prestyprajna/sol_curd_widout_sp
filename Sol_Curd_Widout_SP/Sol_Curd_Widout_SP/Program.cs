﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Curd_Widout_SP
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(async() =>
            {
                try
                {
                    #region  INSERT

                    //var flag = await new UserDal().InsertAsync(new Entity.UserEntity()
                    //{                        
                    //    FirstName = "SAPTHA",
                    //    LastName = "SHREE"
                    //});

                    #endregion

                    #region  UPDATE

                    //var flag = await new UserDal().UpdateAsync(new Entity.UserEntity()
                    //{
                    //    UserId = 5,
                    //    FirstName = "PRAJNA",
                    //    LastName = "SUVARNA"
                    //});

                    #endregion

                    #region  DELETE

                    var flag = await new UserDal().DeleteAsync(new Entity.UserEntity()
                    {
                        UserId = 1                      
                    });

                    #endregion
                }
                catch (Exception ex)
                {

                    Console.WriteLine(ex.Message);
                }
            }).Wait();
        }
    }
}
